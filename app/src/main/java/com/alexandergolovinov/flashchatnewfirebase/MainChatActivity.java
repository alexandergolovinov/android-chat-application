package com.alexandergolovinov.flashchatnewfirebase;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MainChatActivity extends AppCompatActivity {

    // TODO: Add member variables here:
    private String mDisplayName;
    private ListView mChatListView;
    private EditText mInputText;
    private ImageButton mSendButton;
    private DatabaseReference mDatabaseReference;
    private ChatListAdapter mAadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_chat);

        setUpDisplayName();
        //Obtain DB reference storage. It represents particular database reference
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        // Link the Views in the layout to the Java code
        mInputText = (EditText) findViewById(R.id.messageInput);
        mSendButton = (ImageButton) findViewById(R.id.sendButton);
        mChatListView = (ListView) findViewById(R.id.chat_list_view);

        mInputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                sendMessage();
                return true;
            }
        });

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

    }

    //Retrieve display name from SharedPreferences stored in Registration process
    private void setUpDisplayName() {
        SharedPreferences sharedPreferences = getSharedPreferences(RegisterActivity.PREF_USER_DATA, MODE_PRIVATE);
        mDisplayName = sharedPreferences.getString(RegisterActivity.DISPLAY_NAME_KEY, "Anonym");
    }


    private void sendMessage() {
        Log.d("FlashChat", "Message sent! Congrats!");
        String inputChatText = mInputText.getText().toString();
        if (!TextUtils.isEmpty(inputChatText)) {
            InstantMessage chat = new InstantMessage(inputChatText, mDisplayName);
            mDatabaseReference.child("messages").push().setValue(chat);
            mInputText.setText("");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAadapter = new ChatListAdapter(this, mDatabaseReference, mDisplayName);
        mChatListView.setAdapter(mAadapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        mAadapter.cleanUp();
    }

}
